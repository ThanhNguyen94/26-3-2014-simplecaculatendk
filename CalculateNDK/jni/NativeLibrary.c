#include"NativeLibrary.h"
JNIEXPORT jint JNICALL Java_thanh_caulate_ndk_NativeLibrary_addOperation(
		JNIEnv* env, jobject o, jint value1, jint value2) {
	return (jint)(value1 + value2);
}

JNIEXPORT jint JNICALL Java_thanh_caulate_ndk_NativeLibrary_subOperation(
		JNIEnv* env, jobject o, jint value1, jint value2) {
	return (jint)(value1 - value2);
}

JNIEXPORT jint JNICALL Java_thanh_caulate_ndk_NativeLibrary_multiOperation(
		JNIEnv* env, jobject o, jint value1, jint value2) {
	return (jint)(value1 * value2);
}

JNIEXPORT jint JNICALL Java_thanh_caulate_ndk_NativeLibrary_divideOperation(
		JNIEnv* env, jobject o, jint value1, jint value2) {
	return (jint)(value1 / value2);
}
