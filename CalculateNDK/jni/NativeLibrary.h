#include <jni.h>
#include <stdio.h>


JNIEXPORT jint JNICALL Java_thanh_caulate_ndk_NativeLibrary_addOperation(
		JNIEnv* , jobject, jint , jint );

JNIEXPORT jint JNICALL Java_thanh_caulate_ndk_NativeLibrary_subOperation(
		JNIEnv* , jobject, jint , jint);

JNIEXPORT jint JNICALL Java_thanh_caulate_ndk_NativeLibrary_multiOperation(
		JNIEnv* , jobject, jint , jint);

JNIEXPORT jint JNICALL Java_thanh_caulate_ndk_NativeLibrary_divideOperation(
		JNIEnv* , jobject, jint , jint);
